﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Post
    {
        [Key]
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
    }
    public class BlogDBContext : DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }
    }
}