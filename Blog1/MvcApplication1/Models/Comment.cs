﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Comment
    {
        [Key]
        public int ID { set; get; }
        public int PostId { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
    }

}